# ESP32 LED Control

Ce projet permet de contrôler une LED via une interface web en utilisant un ESP32. Vous pouvez allumer et éteindre la LED à partir d'une page web hébergée sur l'ESP32.

## Schéma de câblage

- Connectez la broche **GPIO 5** de l'ESP32 à la cathode (courte patte) de la LED via une résistance de 330Ω.
- Connectez l'anode (longue patte) de la LED à la masse (GND) de l'ESP32.
- Connectez la broche **GND** de l'ESP32 à la masse.

### Vue de la page web # ESP32 LED Control

Ce projet permet de contrôler une LED via une interface web en utilisant un ESP32. Vous pouvez allumer et éteindre la LED à partir d'une page web hébergée sur l'ESP32.

## Schéma de câblage

- Connectez la broche **GPIO 5** de l'ESP32 à la cathode (courte patte) de la LED via une résistance de 330Ω.
- Connectez l'anode (longue patte) de la LED à la masse (GND) de l'ESP32.
- Connectez la broche **GND** de l'ESP32 à la masse.

### Vue de la page web

![ESP32 LED Control Page](web_page.png)

## Instructions

1. Téléchargez et installez le code sur votre ESP32.
2. Connectez-vous à l'adresse IP affichée dans le moniteur série.
3. Utilisez les boutons de l'interface web pour allumer et éteindre la LED.


